/*
 * ������������ ���������������� ������ ����� FOXI � �������� ���������� ����� OpenCV
 *
 * �����������: ��� "��������" <info@xiterra.com>
 */

#include "opencv2/opencv.hpp"

#include <windows.h>

int main()
{
	const char* windowName = "Camera capture";
	cv::VideoCapture vcap(0 + cv::CAP_DSHOW);		
	
	// ��������� ������, ���� ���������	

	// ����� 
	vcap.set(cv::CAP_PROP_FRAME_WIDTH, 1344);
	vcap.set(cv::CAP_PROP_FRAME_HEIGHT, 760);
	
	// ��������
	vcap.set(cv::CAP_PROP_FPS, 160);

	// � ����������, ���� ���� �������������
	//vcap.set(cv::CAP_PROP_EXPOSURE, -7);		

	cv::namedWindow(windowName, cv::WINDOW_NORMAL);
	cv::Mat frame;

	while (true) {

		// ��������� ������ � �����������		
		if (vcap.read(frame)) {
			cv::imshow(windowName, frame);
		}	

		if (cv::waitKey(1) > 0) break;
		if (!IsWindowVisible((HWND)cvGetWindowHandle(windowName))) break;
	}
	
	cv::destroyAllWindows();

	return 0;
}